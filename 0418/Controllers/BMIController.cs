﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _0418.ViewModel;

namespace _0418.Controllers
{
    public class BMIController : Controller
    {
        // GET: BMI
        public ActionResult Index()
        {
            return View(new BMIData());
        }

        [HttpPost]
        public ActionResult Index(BMIData data)
        {

            if (ModelState.IsValid)
            {
                float height_m = data.Height / 100;
                float BMI = data.Weight / (height_m * height_m);
                var level = "";
                if (BMI < 18.5)
                {
                    level = "體重過輕";
                }
                if (BMI > 18.5 && BMI <= 24)
                {
                    level = "正常範圍";
                }
                if (BMI >= 24 && BMI < 27)
                {
                    level = "過重";
                }
                if (BMI >= 27 && BMI < 30)
                {
                    level = "輕度肥胖";
                }
                if (BMI >= 30 && BMI < 35)
                {
                    level = "中度肥胖";
                }
                if (BMI >= 35)
                {
                    level = "重度肥胖";
                }

                data.BMI = BMI;
                data.Level = level;
            }
            //if (data.Height < 50 || data.Height > 200)
            //{
            //    ViewBag.HeightError = "身高請輸入50-200的數值";
            //}

            //if (data.Weight < 30 || data.Weight > 150)
            //{
            //    ViewBag.WeightError = "體中請輸入30-150的數值";
            //}
            

            

            return View(data);
        }
    }
}