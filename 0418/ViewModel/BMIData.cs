﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _0418.ViewModel
{
    public class BMIData
    {
        [Display(Name="體重")]
        [Required(ErrorMessage = "必填欄位")]
        [Range(30,150,ErrorMessage = "請輸入50到150的數值")]
        public float Weight { get; set; }
        [Display(Name = "身高")]
        [Required(ErrorMessage = "必填欄位")]
        [Range(120, 200, ErrorMessage = "請輸入120到200的數值")]
        public float Height { get; set; }
        public float BMI { get; set; }
        public string Level { get; set; }
    }

}